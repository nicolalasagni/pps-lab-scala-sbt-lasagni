import org.scalatest.FlatSpec

/**
  * Created by nicolalasagni on 09/05/2017.
  */

class GraphFlatSpecTest extends FlatSpec {

  "An empty Set" should "have size 0" in {
    assert(Set.empty.size == 0) }

}
